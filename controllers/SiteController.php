<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Entradas;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionListar(){
        $s=Entradas::find()->asArray()->all();
        
        return $this->render("listar",
                [
                    "datos" => $s,
                ]);
    }
    
    public function actionListar1(){
        $s=Entradas::find()->all();
        
        return $this->render("listarTodos",
                [
                    "datos" => $s,
                ]);
    }
    
    public function actionMostrar(){
        $dataProvider=new ActiveDataProvider([
            'query'=>Entradas::find(),
        ]);
        
        return $this->render('mostrar',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMostraruno(){
        return $this->render('mostraUno',[
            'model' => Entradas::findOne(1),
        ]);
    }
}
